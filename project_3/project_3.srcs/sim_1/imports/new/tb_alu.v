module tb_alu();

reg signed  [7:0] tb_datoA, tb_datoB;
reg signed  [5:0] tb_oper;

wire signed  [7:0] tb_aluResult;

integer i;

alu test_unit(
    .i_datoA (tb_datoA), .i_datoB(tb_datoB),             
    .i_oper(tb_oper),
    .o_aluResult(tb_aluResult)
);
    
initial begin
        $dumpfile("dump.vcd");
        $dumpvars;         
        
        tb_datoA = 8'b11111111;
        tb_datoB = 8'b11111111;
        $display("----------> Testing with limit values A: %d | h%h and B: %d | h%h.",tb_datoA,tb_datoA,tb_datoB,tb_datoB);

        tb_oper = 6'b100000;
        #5
        tb_oper = 6'b100010;
        #5
        tb_oper = 6'b100100;
        #5
        tb_oper = 6'b100101;
        #5
        tb_oper = 6'b100110;
        #5
        tb_oper = 6'b000011;
        #5
        tb_oper = 6'b000010;
        #5
        tb_oper = 6'b100111;
        #5 $display("---------> Test finished.");   

    repeat (10) begin   
        tb_datoA = $random;  	
        tb_datoB = $random;
        $display("----------> Testing with random values A: %d | h%h and B: %d | h%h.",tb_datoA,tb_datoA,tb_datoB,tb_datoB);
        
        tb_oper = 6'b100000;      
        #5
        tb_oper = 6'b100010;
        #5
        tb_oper = 6'b100100;
        #5
        tb_oper = 6'b100101;
        #5
        tb_oper = 6'b100110;
        #5
        tb_oper = 6'b000011;
        #5
        tb_oper = 6'b000010;
        #5
        tb_oper = 6'b100111;
        #5  $display("---------> Test finished.");
    end
    
    $finish;
end
  
endmodule
