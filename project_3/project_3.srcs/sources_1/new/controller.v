/* Controller for enter data to ALU */

`timescale 1ns / 1ps
`include "module_alu.v"

module controller
#(
    parameter   NB_DATA = 8,
    parameter   MSB     = NB_DATA - 1,
    parameter   NB_CODE = 6
)
(
    input  clk,
    input signed [MSB:0] i_switchs,    
    input [2:0] buttons,
    output signed [MSB:0] o_resultLeds
    );
    
    reg  signed [MSB:0] o_datoA;
    reg  signed [MSB:0] o_datoB;
    reg  signed [NB_CODE-1:0] o_operCode;
    
    always@(posedge clk)
    begin
        case (buttons) 
            3'b100: o_datoA = i_switchs;
            3'b010: o_datoB = i_switchs;
            3'b001: o_operCode = i_switchs;
            default: $display("Error");
        endcase
    end
    
    alu Alu(
        .i_datoA (o_datoA), .i_datoB(o_datoB),             
        .i_oper(o_operCode),
        .o_aluResult(o_resultLeds)
    );
endmodule
