/* ALU Arithmetic and Logic Operations */

module alu
#(
    parameter   NB_DATA = 8,
    parameter   MSB     = NB_DATA - 1,
    parameter   NB_CODE = 6
)

(
    input   signed  [MSB:0] i_datoA, i_datoB,
    input   signed  [NB_CODE -1:0] i_oper,
    output  signed  [MSB:0] o_aluResult
);

localparam  ADD = 6'b100000;
localparam  SUB = 6'b100010;
localparam  AND = 6'b100100;
localparam  OR  = 6'b100101;
localparam  XOR = 6'b100110;
localparam  SRA = 6'b000011;
localparam  SRL = 6'b000010;
localparam  NOR = 6'b100111;

reg signed [MSB:0] r_aluResult;

assign o_aluResult = r_aluResult;

always @(*)
begin
    case (i_oper)
      ADD: begin r_aluResult = i_datoA + i_datoB; $display("ADD operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      SUB: begin r_aluResult = i_datoA - i_datoB; $display("SUB operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      AND: begin r_aluResult = i_datoA & i_datoB; $display("AND operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      OR:  begin r_aluResult = i_datoA | i_datoB; $display("OR operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      XOR: begin r_aluResult = i_datoA ^ i_datoB; $display("XOR operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      SRA: begin r_aluResult = i_datoA >>> i_datoB ; $display("SRA operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      SRL: begin r_aluResult = i_datoA >> i_datoB; $display("SRL operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      NOR: begin r_aluResult = ~(i_datoA | i_datoB); $display("NOR operation, A: %d | h%h, B: %d | h%h, Result: %d | h%h",i_datoA,i_datoA,i_datoB,i_datoB,r_aluResult,r_aluResult); end
      default: begin $display("Invalid Oper"); end    endcase
end

endmodule
